<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<meta name="generator" content="hevea 2.34">
<meta name="Author" content="Luc Maranget">
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script><link rel="stylesheet" type="text/css" href="manual.css">
<title>Support for style sheets</title>
</head>
<body>
<a href="manual018.html"><img src="previous_motif.svg" alt="Previous"></a>
<a href="manual002.html"><img src="contents_motif.svg" alt="Up"></a>
<a href="manual020.html"><img src="next_motif.svg" alt="Next"></a>
<hr>
<h2 class="section" id="style:sheets">9&#XA0;&#XA0;Support<a id="hevea_default116"></a> for style sheets</h2>
<ul>
<li><a href="manual019.html#sec76">Overview</a>
</li><li><a href="manual019.html#css%3Achange%3Aall">Changing 
the style of all instances of an environment</a>
</li><li><a href="manual019.html#css%3Achange">Changing the style of some instances of an environment</a>
</li><li><a href="manual019.html#whatclass">Which class affects what</a>
</li><li><a href="manual019.html#sec80">A few examples</a>
</li><li><a href="manual019.html#sec84">Miscellaneous</a>
</li></ul>
<h3 class="subsection" id="sec76">9.1&#XA0;&#XA0;Overview</h3>
<p>
Starting with version&#XA0;1.08, H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A offers support for style sheets
(of the CSS variant see&#XA0;[<a href="manual047.html#css">CSS-2</a>]).</p><p>Style sheets provide enhanced expressiveness. For instance, it is now possible
to get &#X201C;real&#X201D; (whatever real means here) small caps in html, and in a
relatively standard manner. There are other, discrete, maybe
unnoticeable, similar enhancements.</p><p>However, style sheets mostly offer an additional mechanism to
customise their documents to H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A users. To do so, users should
probably get familiar with how H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A uses style sheets in the first
place.</p><p>H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A interest for style sheets is at the moment confined to
block-level elements (<code>div</code>, <code>table</code>, <code>H&lt;</code><span class="c019">n</span><code>&gt;</code>,
etc.).
The general principle is as follows: when a command or an
environment gets translated into a block-level element,
the opening tag of the block level element has a
<span class="c013">class="<span class="c019">name</span>"</span> attribute, where <span class="c019">name</span> is the
command or environment name.</p><p>As an example the L<sup>A</sup>T<sub>E</sub>X command <code>\subsection</code>
is implemented with the element&#XA0;<code>h3</code>, resulting in
html output of the form:
</p><pre class="verbatim">    &lt;h3 class="subsection"&gt;
    ...
    &lt;/h3&gt;
</pre><p>
By default, most styles are undefined, and default rendering of
block-level elements applies. However, some packages (such as, for
instance&#XA0;<span class="c013">fancysection</span>, see Section&#XA0;<a href="extras.html#fancysection">B.16.4</a>) may
define them.
<a id="hevea_default117"></a><a id="hevea_default118"></a>If you wish to change the style of section headers, loading the
<span class="c013">fancysection</span> package may prove appropriate (see&#XA0;<a href="extras.html#fancysection">B.16.4</a>).
However, one can also proceed more directly, by appending new
definitions to the <em>document style
sheet</em>, with the command <code>\newstyle</code>.
For instance, here is a <code>\newstyle</code> to add style for subsections.
</p><pre>
  \newstyle{.subsection}{padding:1ex;color:navy;border:solid navy;}
</pre><p>
This declaration adds some style element to the
<span class="c013">subsection</span> class (notice the dot!):
blocks that declare to belong to the class
will show dark-blue text, some padding
(space inside the box) is added and a border will be drawn around the block.
These specification will normally affect all subsections in the document.

Given the previous style definition, the sectioning command
</p><pre class="verbatim">\subsection*{A styled subsection heading}
</pre><p>
should yield:
</p><h3 class="subsectionex">
A styled subsection heading
</h3><p>The following points are worth noticing:
</p><ul class="itemize"><li class="li-itemize">
To yield some effect, <code>\newstyle</code>&#XA0;commands <em>must</em> appear
in the document preamble, <em>i.e.</em> before <code>\begin{document}</code>.
</li><li class="li-itemize">Arguments to <code>\newstyle</code>&#XA0;commands are processed.
</li><li class="li-itemize">The <span class="c013">hevea</span> package defines all style&#XA0;sheet related
commands as no-ops. Thus, these commands do not affect
document processing by L<sup>A</sup>T<sub>E</sub>X.
</li></ul>
<h3 class="subsection" id="css:change:all">9.2&#XA0;&#XA0;Changing 
the style of all instances of an environment</h3>
<p>In this very document, all <span class="c013">verbatim</span> environments appear over
a light green background, with small left and right margins.
This has been performed by simply issuing the following command in
the document preamble.
</p><pre class="verbatim">\newstyle{.verbatim}{margin:1ex 1ex;padding:1ex;background:\#ccffcc;}
</pre><p>
Observe that, in the explicit numerical color argument above, the
hash character &#X201C;<span class="c013">#</span>&#X201D; has to be escaped.</p>
<h3 class="subsection" id="css:change">9.3&#XA0;&#XA0;Changing the style of some instances of an environment</h3>
<p>
<a id="hevea_default119"></a><a id="hevea_default120"></a>One can also change the style class attached to a given instance of
an environment and thus control styling of environments more precisely.</p><p>As a matter of fact, the name of the class attribute of
environment&#XA0;<span class="c019">env</span> is referred to through an indirection, by
using the command <code>\getenvclass{</code><span class="c019">env</span><code>}</code>.
The class attribute can be changed with the command
<code>\setenvclass{</code><span class="c019">env</span><code>}{</code><span class="c019">class</span><code>}</code>.
The&#XA0;<code>\setenvclass</code> command internally defines a command
<code>\</code><span class="c019">env</span><code>@class</code>, whose content is read
by the <code>\getenvclass</code> command. As a consequence, the class
attribute of environments follows normal scoping rules.
<a id="getstylecolor:example"></a>
For instance, here is how to change the style of <em>one</em> <span class="c013">verbatim</span>
environment.

</p><pre class="myverbatim">{\setenvclass{verbatim}{myverbatim}
\begin{verbatim}
This will be styled through class 'myverbatim', introduced by:
\newstyle{.myverbatim}
  {margin:1ex 3x;padding:1ex;
   color:maroon;
   background:\@getstylecolor[named]{Apricot}}
\end{verbatim}}</pre><p>
Observe how the class of environment <span class="c013">verbatim</span> is changed from
its default value to the
new value <span class="c013">myverbatim</span>. The change remains active until the
end of the current group (here, the &#X201C;<span class="c013">}</span>&#X201D; at the end). Then, the class
of environment <span class="c013">verbatim</span> is restored to its default value
&#X2014; which happen to be <span class="c013">verbatim</span>.</p><p><a id="hevea_default121"></a>
This example also shows two new ways to specify colours in style
definition, with a
conventional html&#XA0;color name (here <span class="c009">maroon</span>) or as
a high-level color (see Section&#XA0;<a href="manual036.html#color%3Apackage">B.14.2</a>), given as an argument to
the <code>\@getstylecolor</code> internal command
(here <span style="font-family:monospace;background-color:#FFAD7A">Apricot</span> from the <span class="c013">named</span> color model).</p><p>A good way of specifying style class changes probably is by defining
new environments.
</p><pre class="verbatim">\newenvironment{flashyverbatim}
  {\setenvclass{verbatim}{myverbatim}\verbatim}
  {\endverbatim}
</pre><p>
Then, we can use <code>\begin{flashyverbatim}</code>&#X2026;
<code>\end{flashyverbatim}</code> to get verbatim environments style with
the intended <span class="c013">myverbatim</span> style class.
</p><pre class="myverbatim">This text is typeset inside the environment
\emph{flashyverbatim}, and hence with the \emph{myverbatim}
style.
</pre>
<h3 class="subsection" id="whatclass">9.4&#XA0;&#XA0;Which class affects what</h3>
<p>Generally, the styling of environment&#XA0;<span class="c019">env</span> is performed through
the commands
<code>\getenvclass{</code><span class="c019">env</span><code>}</code>
and <code>\setenvclass{</code><span class="c019">env</span><code>}{</code>&#X2026;<code>}</code>,
with <code>\getenvclass{</code><span class="c019">env</span><code>}</code> producing the
default value of <span class="c019">env</span>.</p><p>Concretely, this means that most of the environments are styled through
an homonymous style class. Here is a non-exhaustive list of such
environments
</p><blockquote class="quote">
figure, table, itemize, enumerate, list, description, 
trivlist, center, flushleft, flushright, quote,
quotation, verbatim, abstract, mathpar (cf
Section&#XA0;<a href="manual-packages.html#mathpartir%3Apackage">B.17.15</a>), lstlisting
(cf. Section&#XA0;<a href="manual-packages.html#listings%3Apackage">B.17.13</a>), etc.
</blockquote><p>All sectioning commands (<code>\part</code>, <code>\section</code> etc.)
output&#XA0;<code>H&lt;</code><span class="c019">n</span><code>&gt;</code> block-level elements, which are styled
through style classes named <span class="c013">part</span>, <span class="c013">section</span>, etc.</p><p>List making-environment introduce extra style classes for items.
More specifically, for list-making environments
<span class="c013">itemize</span> and <span class="c013">enumerate</span>,
<code>li</code> elements are styled as follows:
</p><div class="center">
<table class="c002 cellpading0"><tr><td class="c040"><pre class="verbatim">&lt;ul class="itemize"&gt;
&lt;li class="li-itemize"&gt; ...
&lt;/ul&gt;
</pre></td><td class="c040"><pre class="verbatim">&lt;ol class="enumerate"&gt;
&lt;li class="li-enumerate"&gt; ...
&lt;/ol&gt;
</pre>
</td></tr>
</table>
</div><p>
That is, <code>li</code> elements are styled as environments, the key name
being <span class="c013">li-</span><span class="c019">env</span>.</p><p>The <span class="c013">description</span>, <span class="c013">trivlist</span> and <span class="c013">list</span> environments
(which all get translated into <span class="c013">DL</span> elements) are styled in
a similar way, internal <span class="c013">DT</span> and <span class="c013">DD</span> elements being
styles through names <span class="c013">dt-</span><span class="c019">env</span> and
<span class="c013">dd-</span><span class="c019">env</span> respectively.</p>
<h3 class="subsection" id="sec80">9.5&#XA0;&#XA0;A few examples</h3>
<h4 class="subsubsection" id="sec81">9.5.1&#XA0;&#XA0;The title of the document</h4>
<p><a id="hevea_default122"></a>
The command <code>\maketitle</code> formats the document
title within a <code>table</code> element, with
class <span class="c013">title</span>, for display. The name of the title is displayed
inside block <code>h1</code>, with class <span class="c013">titlemain</span>, while all other
information (author, date) are displayed inside block <code>h3</code>, with class
<span class="c013">titlerest</span>.
</p><pre class="verbatim">&lt;table class="title"&gt;
 &lt;tr&gt;
  &lt;td style="padding:1ex"&gt;
   &lt;h1 class="titlemain"&gt;..title here..&lt;/h1&gt;
   &lt;h3 class="titlerest"&gt;..author here..&lt;/h3&gt;
   &lt;h3 class="titlerest"&gt;..date here..&lt;/h3&gt;
  &lt;/td&gt;
 &lt;/tr&gt;
&lt;/table&gt;
</pre><p>
Users can impact on title formatting by adding style in the
appropriate style classes.
For instance the following style class definitions:
</p><pre class="verbatim">\newstyle{.title}
  {text-align:center;margin:1ex auto;color:navy;border:solid navy;}
\newstyle{.titlerest}{font-variant:small-caps;}
</pre><p>
will normally produce a title in dark blue, centered in a box, with
author and date in small-caps.

</p><table class="xtitle"><tr><td style="padding:1ex;">
<h1 class="xtitlemain">Title</h1>
<h3 class="xtitlerest">Date</h3>
<h3 class="xtitlerest">Author</h3>
</td></tr>
</table>
<h4 class="subsubsection" id="sec82">9.5.2&#XA0;&#XA0;Enclosing things in a styled <span class="c013">div</span></h4>
<p>
<a id="hevea_default123"></a><a id="hevea_default124"></a><a id="hevea_default125"></a>At the moment, due to the complexity of the task, environments
<span class="c013">tabular</span> and <span class="c013">array</span> cannot be styled as others
environments can be, 
by defining an appropriate class in the preamble.
However, even for such constructs,
limited styling can be performed, by using
the <span class="c013">divstyle</span> environment.
The opening command <code>\begin{divstyle}{</code><span class="c019">class</span><code>}</code>
takes the name of a class as
an argument, and translates to <code>&lt;div class="</code><span class="c019">class</span><code>"&gt;</code>.
Of course the closing command <code>\end{divstyle}</code> translates to
<code>&lt;/div&gt;</code>.
The limitation is that the enclosed part may generate more html
blocks, and that not all style attribute defined in class class
<span class="c019">class</span> will apply to those inner blocks.</p><p>As an example consider the style class definition below.
</p><pre class="verbatim">\newstyle{.ruled}{border:solid black;padding:1ex;background:\#eeddbb;color:maroon}
</pre><p>
The intended behaviour is to add a black border around the inner block
(with some padding), and to have <span class="c009">maroon</span> text over
a light brown background.</p><p>If we, for instance, enclose an <span class="c013">itemize</span> environment, the
resulting effect is more or less what we have expected:
</p><pre class="verbatim">\begin{divstyle}{ruled}
\begin{itemize}
\item A ruled itemize
\item With two items.
\end{itemize}
\end{divstyle}
</pre><div class="ruled">
<ul class="itemize"><li class="li-itemize">
A ruled itemize
</li><li class="li-itemize">With two items.
</li></ul>
</div><p>However, enclosing a centered
<span class="c013">tabular</span> environment in a <span class="c013">divstyle{ruled}</span> one 
is less satisfactory.
</p><pre class="verbatim">\begin{divstyle}{ruled}
\begin{center}\begin{tabular}{|c|c|}
\hline \bf English &amp; \bf French\\ \hline
Good Morning &amp; Bonjour\\ Thank You &amp; Merci\\ Good Bye &amp; Au Revoir\\ \hline
\end{tabular}\end{center}
\end{divstyle}
</pre><div class="ruled">
<div class="center"><table class="c001 cellpadding1" border=1><tr><td class="c029"><span class="c023">English</span></td><td class="c029"><span class="c023">French</span></td></tr>
<tr><td class="c029">Good Morning</td><td class="c029">Bonjour</td></tr>
<tr><td class="c029">Thank You</td><td class="c029">Merci</td></tr>
<tr><td class="c029">Good Bye</td><td class="c029">Au Revoir</td></tr>
</table></div>
</div><p>

In the html version of this document,
one sees that the brown background extend on all the width
of the displayed page.</p><p>This problem can be solved by introducing an extra table.
We first open an extra centered table and then only open the
<span class="c013">divstyle</span> environment.
</p><pre class="verbatim">\begin{center}\begin{tabular}{c}
\begin{divstyle}{ruled}
\begin{tabular}{|c|c|}
\hline \bf English &amp; \bf French\\ \hline
Good Morning &amp; Bonjour\\ Thank You &amp; Merci\\ Good Bye &amp; Au Revoir\\
\hline
\end{tabular}
\end{divstyle}
\end{tabular}\end{center}
</pre><p>
This works because of the rules that
govern the width of html <code>table</code> elements, which yield
minimal width. This trick is used in
numerous places by H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A, for instance in document titles, and looks
quite safe.

</p><div class="center"><table class="c002 cellpading0"><tr><td class="c030"><div class="ruled">
<table class="c001 cellpadding1" border=1><tr><td class="c029"><span class="c023">English</span></td><td class="c029"><span class="c023">French</span></td></tr>
<tr><td class="c029">Good Morning</td><td class="c029">Bonjour</td></tr>
<tr><td class="c029">Thank You</td><td class="c029">Merci</td></tr>
<tr><td class="c029">Good Bye</td><td class="c029">Au Revoir</td></tr>
</table>
</div>
</td></tr>
</table></div><p>Another solution is to specify the <span class="c013">display</span> property
of the styling <span class="c013">div</span> block as being <span class="c013">inline-block</span>:
</p><pre class="verbatim">\newstyle{.ruledbis}
  {border:solid black;padding:1ex;background:\#eeddbb;color:maroon;display:inline-block;}
</pre><div class="center">
<div class="ruledbis">
<table class="c001 cellpadding1" border=1><tr><td class="c029"><span class="c023">English</span></td><td class="c029"><span class="c023">French</span></td></tr>
<tr><td class="c029">Good Morning</td><td class="c029">Bonjour</td></tr>
<tr><td class="c029">Thank You</td><td class="c029">Merci</td></tr>
<tr><td class="c029">Good Bye</td><td class="c029">Au Revoir</td></tr>
</table>
</div>
</div>
<h4 class="subsubsection" id="sec83">9.5.3&#XA0;&#XA0;Styling the <span class="c013">itemize</span> environment</h4>
<p>
<a id="hevea_default126"></a>


Our idea is highlight lists with a left border whose color fades
while lists are nested.
Such a design may be appropriate for tables of content, as 
the <a href="manual001.html#ftoc">one of this document</a>.

</p><ul class="toc1"><li class="li-itemize">
Part&#XA0;A
<ul class="toc2"><li class="li-itemize">
Chapter&#XA0;I
<ul class="toc3"><li class="li-itemize">
Section&#XA0;I.1
</li><li class="li-itemize">Section&#XA0;I.2
</li></ul>
</li><li class="li-itemize">Chapter&#XA0;II
<ul class="toc3"><li class="li-itemize">
Section&#XA0;II.1
</li><li class="li-itemize">Section&#XA0;II.2
</li></ul>
</li><li class="li-itemize">Chapter&#XA0;III
</li></ul>
</li><li class="li-itemize">Part&#XA0;B
<ul class="toc2"><li class="li-itemize">
Chapter&#XA0;IV
<ul class="toc3"><li class="li-itemize">
Section&#XA0;IV.1
<ul class="toc4"><li class="li-itemize">
Section&#XA0;IV.1.a
</li><li class="li-itemize">Section&#XA0;IV.1.b
</li></ul>
</li><li class="li-itemize">Section&#XA0;IV.2
</li></ul>
</li><li class="li-itemize">Chapter&#XA0;V
</li></ul>
</li></ul><p>

The text above is typeset from the following L<sup>A</sup>T<sub>E</sub>X source.
</p><pre class="verbatim">\begin{toc}
\item Part~A
\begin{toc}
\item Chapter~I
\begin{toc}
\item Section~I.1
\item Section~I.2
\end{toc}
  ...
\end{toc}
\end{toc}
</pre><p>
For simplicity, we assume a limit of four over the nesting depth of
<span class="c013">toc</span> environment.
We first define four style classes <span class="c013">toc1</span>, <span class="c013">toc2</span>,
<span class="c013">toc3</span> and <span class="c013">toc4</span> in the document preamble.
Since those classes are similar, a command <code>\newtocstyle</code> is
designed.
</p><pre class="verbatim">\newcommand{\newtocstyle}[2]
{\newstyle{.toc#1}{list-style:none;border-left:1ex solid #2;padding:0ex 1ex;}}
\newtocstyle{1}{\@getstylecolor{Sepia}}
\newtocstyle{2}{\@getstylecolor{Brown}}
\newtocstyle{3}{\@getstylecolor{Tan}}
\newtocstyle{4}{\@getstylecolor{Melon}}
</pre><p>
The <span class="c013">toc</span> environment uses a counter to record nesting depth.
Notice how the style class of the <span class="c013">itemize</span> environment is
redefined before <code>\begin{itemize}</code>.
</p><pre class="verbatim">\newcounter{toc}
\newenvironment{toc}
{\stepcounter{toc}\setenvclass{itemize}{toc\thetoc}\begin{itemize}}
{\addtocounter{toc}{-1}\end{itemize}}
</pre><p>
The outputted html is:
</p><pre class="verbatim">&lt;ul class="toc1"&gt;&lt;li class="li-itemize"&gt;
Part&amp;nbsp;A
&lt;ul class="toc2"&gt;&lt;li class="li-itemize"&gt;
Chapter&amp;nbsp;I
&lt;ul class="toc3"&gt;&lt;li class="li-itemize"&gt;
Section&amp;nbsp;I.1
&lt;li class="li-itemize"&gt;Section&amp;nbsp;I.2
  ...
&lt;/ul&gt;
&lt;/ul&gt;
</pre>
<h3 class="subsection" id="sec84">9.6&#XA0;&#XA0;Miscellaneous</h3>
<h4 class="subsubsection" id="sec85">9.6.1&#XA0;&#XA0;H<span class="c016"><sup>A</sup></span>C<span class="c016"><sup>H</sup></span>A and style sheets</h4>
<p>
H<span class="c016"><sup>A</sup></span>C<span class="c016"><sup>H</sup></span>A now produces an additional file: a style sheet, which is
shared by all the&#XA0;html files produced by H<span class="c016"><sup>A</sup></span>C<span class="c016"><sup>H</sup></span>A.
Please refer to section&#XA0;<a href="cutname.html#hacha%3Astyle">7.1</a> for details.</p>
<h4 class="subsubsection" id="sec86">9.6.2&#XA0;&#XA0;Producing an external style sheet</h4>
<p>
<a id="hevea_default127"></a><a id="hevea_default128"></a>By default, style declarations defined with
<code>\newstyle</code> go into the header of the html document
<em>doc</em><span class="c013">.html</span>.
However, one can send those declaration into an external style file,
whose name is <em>doc</em><span class="c013">.css</span>.
Then, H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A automatically relates <em>doc</em><span class="c013">.html</span> to
its style sheet <em>doc</em><span class="c013">.css</span>.
To achieve this behaviour, it suffices to set the value of the boolean
register <span class="c013">externalcss</span> to <span class="c019">true</span>, by issuing the command
<code>\externalcsstrue</code> in the preamble of the source document.
Notice that H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A output still can be processed by H<span class="c016"><sup>A</sup></span>C<span class="c016"><sup>H</sup></span>A, with
correct behaviour.</p>
<h4 class="subsubsection" id="sec87">9.6.3&#XA0;&#XA0;Linking to external style sheets</h4>
<p>
<a id="hevea_default129"></a><a id="hevea_default130"></a>The H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A command <code>\loadcssfile{</code><span class="c019">url</span><code>}</code> allows the
user to link to an external style sheet (like the link option for
HTML). The command takes an <span class="c019">url</span> of the external
sheet as argument and emits the HTML text to
<em>link</em> to the given external style sheet. As an example, the command
</p><pre class="verbatim">\loadcssfile{../abc.css}
</pre><p>
produces the following html text in the <code>head</code> of the document.
</p><pre class="verbatim">  &lt;link rel="stylesheet" type="text/css" href="../abc.css"&gt;
</pre><p> 
To yield some effect, <code>\loadcssfile</code> must appear in the document
preamble. Several <code>\loadcssfile</code> commands can be issued. Then
the given external style sheets appear in the output, following source
order.</p><p>Notice that the argument to <code>\loadcssfile</code> is processed. Thus, if it
contains special characters such as &#X201C;#&#X201D; or &#X201C;$&#X201D;, those must be specified
as <code>\#</code> and <code>\$</code> respectively.
A viable alternative would be to quote
the argument using the <code>\url</code> command from the <span class="c013">url</span>
package (see Section&#XA0;<a href="manual-packages.html#urlpackage">B.17.11</a>).</p>
<h4 class="subsubsection" id="sec88">9.6.4&#XA0;&#XA0;Limitations</h4>
<p>
At the moment, style class definitions cumulate, and appear
in the <code>style</code> element in the order they are given in the
document source. There is no way to cancel the default class
definitions performed by H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A before it starts to process the
user&#X2019;s document.
Additionally, external style sheets specified with <code>\loadcssfile</code>
appear before style classes defined with <code>\newstyle</code>.
As a consequence (if I am right), styles
declared by <code>\newstyle</code> take precedence over those contained in
external style sheets. Thus, using external style-sheets, especially
if they alter the styling of elements, may produce awkward results.</p><p>Those limitations do not apply of course to style classes whose
names are new, since there cannot be default definitions for them.
Then, linking with external style sheets can prove useful to
promote uniform styling of several documents produced by H<span class="c016"><sup>E</sup></span>V<span class="c016"><sup>E</sup></span>A.</p>
<hr>
<a href="manual018.html"><img src="previous_motif.svg" alt="Previous"></a>
<a href="manual002.html"><img src="contents_motif.svg" alt="Up"></a>
<a href="manual020.html"><img src="next_motif.svg" alt="Next"></a>
</body>
</html>
